<!DOCTYPE html>
<?php session_start();
if (!isset($_SESSION['id'])){
    die ('You must connect with your login and password');
}
?>
<html lang="fr en">

<head>
    <meta charset="utf-8">
    <title>YouMix</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/3e92aa8b45.js"></script>
    <link rel="stylesheet" href="assets/style_video_list.css">
    <link href="https://fonts.googleapis.com/css?family=Leckerli+One|Roboto" rel="stylesheet">
</head>

<body>
    <div class="container_column">
        <header class="header">
            <a href='index.php'>
                <span>
                    <h1>
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    </h1>
                    <h1> You Mix</h1>
                </span>
            </a>
            <a href='form/logout.php'>
                <span>
                    <h3>
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    </h3>
                    <h3> Logout</h3>
                </span>
            </a>

        </header>
        <main class="main">
            <div class="container_row">
                <div class="video">
                    <h2><?php echo $_SESSION['list_name']?></h2>
                    <div id="player"></div>
                </div>
                <div class="list">
                    <?php
                include('class/user.php');
                $req=newUser::connect_user($_SESSION['id']);
                $value=$req->fetchAll();
                    ?>
                        <div class="mix_name">
                            <h2><?php echo $_SESSION['user_login'].'\'s list'?></h2>
                        </div>
                        <ul>
                            <?php foreach($value as $data){
                                $video_id=$data['link'];//store the vidéo id
                                $video_list[]=$video_id;//store the id in a array
                                $arr_vid=json_encode($video_list);//encode in JS
                                ?>
                            <li>
                                <span>
                                    <a href="#" class=" play_video link" data-id=<?php echo $data[ 'link'];?>>
                                        <img src="http://img.youtube.com/vi/<?php echo $data['link']; ?>/2.jpg" alt="video thumbnail">
                                    </a>
                                </span>
                                <div class="container_column_list">
                                    <span>
                                        <?php echo $data['title']; ?>
                                    </span>
                                    <span>
                                        <button class="play_video button" type="submit" name="play" data-id=<?php echo $data[ 'link'];?>>
                                            <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                                        </button>
                                        <form action="form/delete_video.php" method="POST">
                                            <input type="hidden" name="id" value="<?php echo $data['id'];?>" />
                                            <button class="button delete_video" type="submit" name="play">
                                                <i class="fa fa-trash-o" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </span>
                                </div>
                            </li> <?php } ?>
                        </ul>
                        <div class="addLink">
                            <form action="form/add_video.php" method="POST">
                                <input type="hidden" name="user_id" value="<?php echo $data['user_id'];?>">
                                <input type="text" name="link" placeholder="add some music to the mix...">
                                <button type="submit" class="add_video" value="Add">
                                    <i class="fa fa-plus" aria-hidden="true"></i>
                                </button>
                            </form>
                        </div>
                </div>
            </div>
        </main>
    </div>
    <script>

        //Google YouTupe Player API
        // 2. This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');
        var video_list=<?php echo $arr_vid ?>;
        var cue_video=video_list[0];
        var i=0;
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

        // 3. This function creates an <iframe> (and YouTube player)
        //    after the API code downloads.
        var player;
        function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
                height: '360',
                width: '640',
                videoId: cue_video,
                events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                }
            });
        }

        // 4. The API will call this function when the video player is ready.
        function toArray(nodelist) {
            return ([].slice.call(nodelist))
        }
        function onPlayerReady(event) {
            var play = document.querySelectorAll(".play_video");
            play.forEach(function (button) {
                button.addEventListener("click", function (event) {
                    player.loadVideoById(this.dataset.id);
                    player.playVideo();
                });
            });
            var pause = document.querySelectorAll(".pause_video");
            pause.forEach(function (button) {
                button.addEventListener("click", function (event) {
                    player.pauseVideo();
                });
            });
        }

        // 5. The API calls this function when the player's state changes.
        //    The function indicates that when playing a video (state=1),
        //    the player should play for six seconds and then stop.
        function onPlayerStateChange(event) {
            if (event.data == YT.PlayerState.ENDED) {
                cue_video=video_list[++i];
                player.loadVideoById(cue_video);
            }
        }
        function stopVideo() {
            player.stopVideo();
        }
    </script>
</body>

</html>