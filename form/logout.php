<!DOCTYPE html>
<html lang="fr en">
<head>
    <meta charset="utf-8">
    <title>YouMix</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/3e92aa8b45.js"></script>
    <link rel="stylesheet" href="../assets/style_video_list.css">
    <link rel="stylesheet" href="../assets/style_loader.css">
    <link href="https://fonts.googleapis.com/css?family=Leckerli+One|Roboto" rel="stylesheet">
</head>
<body>
    <div class="container_column">
        <header class="header">
            <a href='index.php'>
                <span>
                    <h1>
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    </h1>
                    <h1> You Mix</h1>
                </span>
            </a>
        </header>
        <div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>
<?php
echo 'Déconnexion ...';
session_start();
session_destroy();
header("refresh:2;url=../index.php");
?>