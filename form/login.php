<!DOCTYPE html>
<html lang="fr en">
<head>
    <meta charset="utf-8">
    <title>YouMix</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/3e92aa8b45.js"></script>
    <link rel="stylesheet" href="../assets/style_video_list.css">
    <link rel="stylesheet" href="../assets/style_loader.css">
    <link href="https://fonts.googleapis.com/css?family=Leckerli+One|Roboto" rel="stylesheet">
</head>
<body>
    <div class="container_column">
        <header class="header">
            <a href='index.php'>
                <span>
                    <h1>
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    </h1>
                    <h1> You Mix</h1>
                </span>
            </a>
        </header>
        <div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>
<?php
include('../db/config.php');
$login=$_POST ["pseudo"];
$pwd= hash ('sha256',$_POST ["password"]);

//Fetch id, pwd and name of the list
$bdd = getConnection();
$req = $bdd->prepare('SELECT id , user_login, list_name FROM user WHERE pwd=:pwd AND user_login =:user_login');
$req->execute(array(
    'user_login' => $login,
    'pwd' => $pwd));

$result = $req->fetch();
// Compare id and pwd to database
if (!$result)
{
    echo '<h3>Mauvais identifiant ou mot de passe !</h3>';
    header("refresh:2;url=../index.php");
}
else
{
    session_start();
    $_SESSION['id'] = $result['id'];
    $_SESSION['user_login'] = $login;
    $_SESSION['list_name'] = $result['list_name'];;
    echo '<h3>Vous êtes connecté !</h3>';
    header("refresh:2;url=../youmix.php");

}
?>