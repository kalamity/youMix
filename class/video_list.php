<?php
// List :  CRUD function (add&delete)
include_once('../db/config.php');//Connection to the database
class myList {
//Add a Video
    public static function add($link, $title, $user_id){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO video_link (user_id,link,title) VALUES (:user_id,:link, :title)');
        $req->execute(['user_id'=> $user_id,'link' => $link, 'title' => $title]);
    }


//Add a Video Title
    static function addTitle($title){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO video_link(title) VALUES (:title)');
        $req->execute(array('title' => $title));
}
//Delete
    static function delete($id){
        $bdd = getConnection();
        $req = $bdd->prepare('DELETE FROM video_link WHERE id=:id');
        $req->execute(['id'=>$id]);
    }
}
?>