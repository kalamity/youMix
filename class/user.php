<?php
include_once(__DIR__.'/../db/config.php');//Connection to the database
class newUser {
//Add a USer
    public static function add_user($login,$list,$pwd){
        $bdd = getConnection();
        $req = $bdd->prepare('INSERT INTO user (user_login,list_name,pwd) VALUES (:user_login,:list_name,:pwd)');
        $req->execute(['user_login' => $login,'list_name' => $list,'pwd' => $pwd ]);
        return $bdd->lastInsertId();//pour réqupérer l'id 
    }
//Delete a User
    static function delete_user($id){
        $bdd = getConnection();
        $req = $bdd->prepare('DELETE FROM user WHERE id=:id');
        $req->execute(['id'=>$id]);

    }

//Connect User to they list of video
    static function connect_user($user_id){
        $bdd = getConnection();
        $req = $bdd->prepare('SELECT * FROM video_link WHERE user_id=:user_id');
        $req->execute(['user_id' => $user_id]);
        return $req;
    }

}
?>