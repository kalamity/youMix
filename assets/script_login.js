//Swithc from the login div to the register div on button click
$(document).ready(function () {

 function hideDiv(){
    $("#toggle" ).on("click",function () {
        $("#login").hide();
        $("#new_account").show();
 });}

 function checkPass()
{
    var password = document.getElementById("password")
    , confirm_password = document.getElementById("confirm_password");
  
  function validatePassword(){
    if(password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
      confirm_password.setCustomValidity('');
    }
  }
  
  password.onchange = validatePassword;
  confirm_password.onkeyup = validatePassword;
}  

 hideDiv();
 checkPass()
});

