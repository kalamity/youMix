// expressions régulières
var regexA = /https?:\/\/www\.youtube\.com\/watch\?v=([A-Za-z0-9\-\_]{11})($|[^A-Za-z0-9\-\_])/
var regexB = /https?\:\/\/youtu\.be\/([A-Za-z0-9\-\_]{11})($|[^A-Za-z0-9\-\_])/

// check si l'URL est au format A
function checkA(str) {
	return regexA.test(str)
}

// check si l'URL est au format B
function checkB(str) {
	return regexB.test(str)
}

// Extraire l'ID youtube pour les URLs de format A
function extractA(str) {
	return regexA.exec(str)[1]
}

// Extraire l'ID youtube pour les URLs de format B
function extractB(str) {
	return regexB.exec(str)[1]
}

// Extraire l'ID d'une vidéo youtube quelque soit le format A ou B
// retourne false si l'URL n'est pas au bon format
function extract(str) {
	if (checkA(str)) {
  	return extractA(str);
  }
  else if (checkB(str)) {
  	return extractB(str);
  }
  else {
  	return false;
  }
}