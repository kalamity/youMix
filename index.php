<!DOCTYPE html>
<html lang="fr en">

<head>
    <meta charset="utf-8">
    <title>YouMix - New User</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://use.fontawesome.com/3e92aa8b45.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Leckerli+One|Roboto" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy"
        crossorigin="anonymous">
    <link rel="stylesheet" href="assets/style_login.css">
</head>

<body>
    <main class="main">
        <div class="header">
                <h1>
                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                </h1>
                <h1> You Mix</h1>
        </div>
        <div id="login" class="box row justify-content-md-center">
            <div>
                <h3 class="title">Login into You Mix </h3>
                <i>For a sneak peek try Anne / chat </i>
            </div>
            <form action="form/login.php"method="POST">
                <div class="form-group">
                    <label for="Pseudo">User Name</label>
                    <input type="text" class="form-control" name="pseudo" placeholder="Choose a user name"required="required">
                <div class="form-group">
                    <label for="Confirme Your Password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                </div>
                <button type="submit" class="btn btn-secondary">Login</button>
            </form>
            <a  id="toggle" class="link" href="#"> or create your account</a>
            </div>
        </div>
        <div id="new_account" class=" hide box row justify-content-md-center">
            <div>
                <h3 class="title">New to You Mix ? </h3>
                <h4 class="text"> Create your account : </h4>
            </div>
            <form action="form/add_user.php"method="POST">
                <div class="form-group">
                    <label for="Pseudo">User Name</label>
                    <input type="text" class="form-control" name="pseudo" placeholder="Choose a user name"required="required">
                </div>
                <div class="form-group">
                    <label for="Mix">Play List Name</label>
                    <input type="text" class="form-control" name="list" placeholder="Choose a name for your playlist"required="required">
                </div>
                <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input id="password"type="password" class="form-control" name="password" placeholder="Password" required="required">
                </div>
                <div class="form-group">
                    <label for="Confirme Your Password">Confirm Password</label>
                    <input id="confirm_password" type="password" class="form-control" name="confirm_password" placeholder="Password" required="required">
                </div>
                <button type="submit" class="btn btn-secondary">Submit</button>
                <a  href="index.php" id="toggle" class="link" href="#"> or log in with your account</a>
            </form>
        </div>
    </main>
    <script src="assets/script_login.js"></script>
</body>

</html>